	var currentUser = {
		id: null,
		username: null,
		password:null
	};

	
	var app_name = 'Metajua',
	app_version = '0.0.1'
	app = new Framework7( {
	    modalTitle: app_name,
	    material: true,
		swipePanel: 'left',
		swipePanelCloseOpposite: true,
		panelsCloseByOutside: true,
		swipePanelOnlyClose: true,
		animatePages: false
	} ),
	pagination = {
		per_page: 1,
		page: 1
	},
	$$ = Dom7,
	db = window.openDatabase( 'metajua_app', app_version, 'Metajua App', 50000 );
	api_base = 'http://isaac.metajua.gq/';





if( window.localStorage.getItem( 'currentUser' )!= null ){
	currentUser = JSON.parse( window.localStorage.getItem( 'currentUser' ) );
	
}

$$( '.app_name' ).text( app_name );

document.addEventListener( 'deviceready', function() {

	if ( window.localStorage.getItem( 'account_setup' ) == null ){
		setup_account();
	}
	else {
			if( currentUser.id == null ){
				openLoginScreen();
			 }
		}

			 
		show_modules();
	
		
}, false );




function show_modules(){
	var account = parseInt( window.localStorage.getItem( 'account_id' ) ).toFixed( 1 );

	db.transaction( function( tx ){
				tx.executeSql(
					'select distinct `name` from ( select a.id, singular, `name`,meta_key,meta_value from  taxonomies  a left join terms b on( a.id =  b.taxonomy_id )left join term_meta c on( c.term_id = b.id ) where singular = "Module" and meta_key = "account" and meta_value = ? ) t',
					[  account  ],
					function( tx, result ){
						var html_elements = '';
						//console.log( result.rows.item( 0 ).name );

						for( i=0; i < result.rows.length; i++ ){

							//console.log( result.rows.item(i) );
							
							$$.each( result.rows.item(i), function( key, value ){
								console.log( value );
									html_elements = '	<li class="item-content"> '+
														+ '<div class="item-media"><i class="icon icon-f7"></i></div> '+
														+ '<div class="item-inner">'+
																+ '<div class="item-title">' + '' + value + '' + ' </div> '+	
																+ '</div> '+
															+ '</li>';
							});

							
							
						}

						$$('.modules_list').html( html_elements );
					},
					function(){
						console.log( 'error ocurred');
					}
				);
	}, null);

	
											
											
	//$$( '.modules_section').html( html_elements );
}



function setup_account() {
	app.popup( '.popup-account', false, true );

	$$( '#account_submit' ).off( 'click' ).on( 'click', function( $event ) {
		var account_name = $$( '#account_name' );

		if ( account_name.val() == '' )
			account_name.focus();

		else {
			app.showPreloader();

			$$.post(
				api_base + 'account/validate',
				{
					account: account_name.val()
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'successful' ) {
						window.localStorage.setItem( 'account_id', response.account_id );
						window.localStorage.setItem( 'account_setup', 1 );

						create_database_tables();

						
					}

					else {
						app.hidePreloader();

						app.alert( 'Account not known. Try again!' );
					}
				},
				function() {
					app.hidePreloader();

					app.alert( 'Server not accessible. Try again later!' );
				}
			);
		}
	} );
}

function create_database_tables() {
	db.transaction( function( tx ) {
		tx.executeSql( 'CREATE TABLE users ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, username CHAR(20), password CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE user_meta ( id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, meta_key CHAR(250), meta_value TEXT )' );
		tx.executeSql( 'CREATE TABLE taxonomies ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, singular CHAR(20), plural CHAR(30) )' );
		tx.executeSql( 'CREATE TABLE terms ( id INTEGER PRIMARY KEY AUTOINCREMENT, taxonomy_id INTEGER, remote_id INTEGER, user_id INTEGER, name CHAR(20) )' );
		tx.executeSql( 'CREATE TABLE term_meta ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, term_id INTEGER, meta_key CHAR(40), meta_value TEXT )' );

		tx.executeSql( 'INSERT INTO taxonomies  ( singular, plural ) VALUES ( ?,? ) ',[ 'Module', 'Modules' ], function( tx, result ){ console.log( result ); }, null );

	} );



	setTimeout( function() { download_users(); }  , 1000);
}

//check if the tax exists
function check_taxonomy_exists( singular ){
	

	db.transaction( function( tx ){
		tx.executeSql( 
			'SELECT * FROM taxonomies WHERE singular = ? LIMIT 1',
			[ singular ],
			function( tx, results ){
				if( results.rows.length == 1){
					result = true;
				
				}
				else{
					result = false;
				}
			},
			null
		);
	});


var interval = setInterval( function(){
			if( typeof result != 'undefined' ){
				clearInterval(  interval );
				console.log( result );
				return result; 
			  }

		}, 5);
	
}


function update_term( taxonomy_id, name, term_meta, remote_id ){
	term_meta = typeof term_meta == 'undefined' ? {} : term_meta;
	remote_id = typeof remote_id == 'undefined' ? null : remote_id;

			//insert into terms
			db.transaction( function( tx ){
				tx.executeSql( 'SELECT id FROM terms WHERE taxonomy_id = ? AND name = ? LIMIT 1', [ taxonomy_id, name ], 
				   function( tx, result ){
					if( result.rows.length == 1) {
						tx.executeSql( 'UPDATE terms SET name = ?, remote_id = ? WHERE id = ?', [ name, remote_id, result.rows.item( 0 ).id ] );
					
						$$.each( term_meta, function( meta_key, meta_value ){
							update_term_meta( result.rows.item( 0 ).id, meta_key, meta_value );
						} );
					}
					
					else
					    tx.executeSql( 'INSERT INTO terms( taxonomy_id, name, remote_id ) VALUES( ?, ?, ? )',[ taxonomy_id, name, remote_id ],
					function( tx, result ) {

						//insert into term_meta
						$$.each( term_meta, function( meta_key, meta_value ){
							update_term_meta( result.insertId, meta_key, meta_value );
						} );
					},
					null  );
				  	} );
			} );
		  
}




function update_term_meta( term_id, meta_key, meta_value ) {
	db.transaction( function( tx ){
		tx.executeSql( 'SELECT id FROM term_meta WHERE term_id = ? AND meta_key = ? LIMIT 1', [ term_id, meta_key ], function( tx, result ) {
			if ( result.rows.length == 1 )
				tx.executeSql( 'UPDATE term_meta SET meta_value = ? WHERE term_id = ? AND meta_key = ?', [ meta_value, term_id, meta_key ] );

			else
				tx.executeSql( 'INSERT INTO term_meta( term_id, meta_key, meta_value ) VALUES( ?, ?, ? )', [ term_id,  meta_key, meta_value ] );
		} );			
	});
}




//modules 
function download_modules() {


	$$.post(
		api_base + 'modules',
		{
			account: window.localStorage.getItem( 'account_id' ),
			per_page: pagination.per_page,
			page: pagination.page
		},
		function( response ) {
			

			var response = JSON.parse( response );

			if ( response.result == 'successful' ) {
				var taxonomy = response.taxonomy;

				db.transaction( function( tx ) {
					tx.executeSql( 'SELECT id FROM taxonomies WHERE singular = ? LIMIT 1', [ taxonomy.singular ], function( tx, result ) {
						if ( result.rows.length == 1 ) {
							$$.each( response.modules, function( index, module ) {
								update_term( result.rows.item( 0 ).id, module.name, { 
									account: module.account 
								} );
							});
						}

						else {
							tx.executeSql( 'INSERT INTO taxonomies (singular, plural) VALUES ( ?, ? )', [ taxonomy.singular, taxonomy.plural ], function( tx, result ) {
								$$.each( response.modules, function( index, module ) {
									update_term( result.insertId, module.name, { 
										account: module.account 
									} );
								});
								
							} );
						}							
					} );					
				} );

				pagination.page = ( response.modules.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );

				if ( pagination.per_page == response.modules.length )
					download_modules();

				else {
					app.hidePreloader();

					app.closeModal( '.popup-account', true );

					
				}
			}

			else
				download_modules();
		},


		function() {
			app.hidePreloader();

			app.alert( 'Server not accessible. Try again later!' );
		}
	);


}






function download_users() {
	if ( pagination.page == 1 ) {
		truncate_table( 'users' );

		truncate_table( 'user_meta' );
	}

	$$.post(
		api_base + 'users',
		{
			account: window.localStorage.getItem( 'account_id' ),
			per_page: pagination.per_page,
			page: pagination.page
		},
		function( response ) {
			var response = JSON.parse( response );

			if ( response.result == 'successful' ) {
				db.transaction( function( tx ) {
					$$.each( response.users, function( index, user ) {
						if ( user.meta.has_mobile_app_access == '1' ) {
							tx.executeSql(
								'INSERT INTO users ( remote_id, username, password ) VALUES ( ?, ?, ? )',
								[ user.id, user.username, user.meta.mobile_app_password ],
								function( tx, result ) {
									var user_meta = Object.assign( {
											first_name: user.name.first,
											last_name: user.name.last
										}, user.meta ),
										user_id = result.insertId;

									$$.each( user_meta, function( meta_key, meta_value ) {
										update_user_meta( user_id, meta_key, meta_value );
									} );

									
								}
							);
						}
					} );
				} );

				pagination.page = ( response.users.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );

				if ( pagination.per_page == response.users.length )
						download_users();

				else {

					setTimeout( function() {  
						download_modules();  
					}, 1000 );

					app.hidePreloader();

					app.closeModal( '.popup-account', true );

					openLoginScreen();
				}
			}

			else
				download_users();
		},
		function() {
			app.hidePreloader();

			app.alert( 'Server not accessible. Try again later!' );
		}  );

		
		

}




function update_user_meta( user_id, meta_key, meta_value ) {
	user_id = typeof user_id == 'undefined' ? 0 : parseInt( user_id );
	meta_value = typeof meta_value == 'undefined' ? '' : meta_value;

	if ( typeof meta_key != 'undefined' ) {
		db.transaction( function( tx ) {
			tx.executeSql(
				'SELECT id FROM user_meta WHERE user_id = ? AND meta_key = ? LIMIT 1',
				[ user_id, meta_key ],
				function( tx, result ) {
					if ( result.rows.length == 1 )
						tx.executeSql(
							'UPDATE user_meta SET meta_value = ? WHERE id = ?',
							[ meta_value, result.rows.item( 0 ).id ]
						);

					else
						tx.executeSql(
							'INSERT INTO user_meta ( user_id, meta_key, meta_value ) VALUES ( ?, ?, ? )',
							[ user_id, meta_key, meta_value ],
							null,
							function( tx, error ) {
								app.alert( error.message );
							}
						);
				}
		 	);
		} );
	}
}

function truncate_table( table ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'DELETE FROM ' + table, [] );
	} );
}

function show_table_records( table ) {
	table = typeof table == 'undefined' ? 'users' : table;

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT * FROM ' + table, [], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				console.log( result.rows.item( i ) );
		} );
	} )
}




function openLoginScreen() {
	app.loginScreen( '.login-screen',true );


	$$( '#login_submit' ).off( 'click' ).on( 'click', function(  ) {
		

		var username = $$( 'input#username' ),
			password = $$( 'input#password' );

			

		if ( username.val().trim().length == 0 )
		 	username.focus();

		else if ( password.val().trim().length == 0 )
			password.focus();

		else {
			

			db.transaction( function( tx ) {
				tx.executeSql(
					'SELECT * FROM users WHERE username = ? LIMIT 1',
					[ username.val() ],
					function( tx, result ) {

						if ( result.rows.length > 0 ) {
							tx.executeSql(
								'SELECT * FROM users WHERE username = ? AND password = ? LIMIT 1',
								[ username.val(), password.val() ],

								function( tx, result ) {
									app.hidePreloader();

									if ( result.rows.length > 0 ) {
										var record = result.rows.item( 0 );

										currentUser.id = record.id;
										currentUser.username = record.username;
										currentUser.password = record.password;
										

										window.localStorage.setItem( 'currentUser', JSON.stringify( currentUser ) );

										

										setTimeout( function() {
											app.closeModal( '.login-screen', true );
										}, 2000 );
									}

									else {
										
											password.val( '' );
											app.alert( 'Wrong password entered. Try again!' );
											password.focus();

										
									}
								},
								null
							);
						}
						else {
							username.val( '' );
							password.val( '' );
							app.alert( 'Wrong username entered' );
							username.focus();
							}

							
					},
					null
				);
			} );
		}
	} );

	
}
