var app_name = 'Metajua',
	app = new Framework7( {
	    modalTitle: app_name,
	    material: true,
		swipePanel: 'left',
		swipePanelCloseOpposite: true,
		panelsCloseByOutside: true,
		swipePanelOnlyClose: true,
		animatePages: false
	} ),
	$$ = Dom7,
	api_base = 'http://isaac.metajua.gq/app/public/';

$$( '.app_name' ).text( app_name );

document.addEventListener( 'deviceready', function() {
}, false );

if ( window.localStorage.getItem( 'account_setup' ) == null )
	setup_account();

function setup_account() {
	app.popup( '.popup-account', false, true );

	$$( '#account_submit' ).off( 'click' ).on( 'click', function( $event ) {
		var account_name = $$( '#account_name' );

		if ( account_name.val() == '' )
			account_name.focus();

		else {
			app.showPreloader();

			$$.post(
				api_base + 'account/validate',
				{
					account: account_name.val()
				},
				function( response ) {
					var response = JSON.parse( response );

					setTimeout( function() {
						app.hidePreloader();

						if ( response.result == 'successful' ) {
							window.localStorage.setItem( 'account_id', response.account_id );
							window.localStorage.setItem( 'account_setup', 1 );

							app.closeModal( '.popup-account', true );
						}

						else
							app.alert( 'Account not known. Try again!' );
					}, 3000 );
				},
				function() {
					setTimeout( function() {
						app.hidePreloader();

						app.alert( 'Server not accessible. Try again later!' );
					}, 3000 );
				}
			);
		}
	} );
}
